#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[link(name = "test")]

fn main() {
    println!("Hello, world!");


    let mut msg_data: [u8; 8] = [65, 65, 65, 65, 65, 65, 65, 65];
    println!("msg data size: {}", msg_data.len());

    let new_data = msg_data.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let _msg = GoSlice {
        data: new_data,
        len: 8,
        cap: 8,
    };
    ::std::mem::forget(msg_data);

    let mut sig_data : [u8; 64] = [12, 55, 96, 75, 135, 170, 226, 65, 92, 177, 149, 103, 206, 235, 32, 26, 234, 202, 163, 201, 33, 5, 13, 3, 194, 57, 105, 228, 81, 96, 221, 8, 203, 244, 102, 47, 182, 185, 68, 33, 240, 122, 176, 34, 31, 25, 101, 238, 157, 205, 181, 216, 111, 75, 140, 78, 52, 198, 153, 35, 162, 186, 110, 116];

    println!("sig data size: {}", sig_data.len());

    let new_sig = sig_data.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let _sig = GoSlice {
        data: new_sig,
        len: 64,
        cap: 64,
    };
    ::std::mem::forget(sig_data);

    let mut n_data : [u8; 64] = [110, 236, 243, 255, 128, 22, 13, 128, 1, 255, 122, 172, 205, 127, 55, 246, 135, 124, 28, 6, 82, 141, 152, 110, 181, 127, 61, 103, 94, 72, 193, 225, 234, 23, 113, 39, 7, 7, 45, 255, 158, 113, 217, 33, 133, 240, 154, 243, 238, 223, 205, 155, 4, 17, 76, 8, 112, 117, 107, 71, 166, 228, 84, 85];
    println!("n data size: {}", n_data.len());
    let new_n = n_data.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let _n = GoSlice {
        data: new_n,
        len: 64,
        cap: 64,
    };

    let e : ::std::os::raw::c_int = 3;

    unsafe {
    let res = VerifyMessage(_msg, _sig, _n, e);

    println!("{}", res)
    }



}

// pub fn VerifyMessage(
// msg: GoSlice (byte),
// signature: GoSlice(byte),
// n: GoSlice(byte),
// e: ::std::os::raw::c_int,
// -> GoUint8;
