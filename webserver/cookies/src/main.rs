#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;

#[cfg(test)]
mod tests;

use std::collections::HashMap;

use rocket::request::Form;
use rocket::response::Redirect;
use rocket::http::{Cookie, Cookies};
use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
extern crate raster;
use raster::Image;
use raster::Color;

#[derive(FromForm)]
struct Message {
    message: String,
}
//takes in message typed from user and adds this message to the cookie
#[post("/submit", data = "<message>")]
fn submit(mut cookies: Cookies, message: Form<Message>) -> Redirect {
    cookies.add(Cookie::new("message", message.into_inner().message));
    Redirect::to("/zippyPage")
}

#[get("/")]
fn index(cookies: Cookies) -> Template {
    let cookie = cookies.get("message");
    let mut context = HashMap::new();
    if let Some(ref cookie) = cookie {
        context.insert("message", cookie.value());
    }

    Template::render("index", &context)
}

#[get("/zippyPage")]
fn zippyPage(cookies: Cookies) -> Template {

    
    use raster::editor;
    //Create an image from file
    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/Rust_Projects/generate-zippy/examples/zippy.png").unwrap();

    image.set_pixel(4,4, Color::red()).unwrap();
    // Save it
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/Rust_Projects/generate-zippy/zippy2.png");
    
    let cookie = cookies.get("message");
    let mut context = HashMap::new();
    if let Some(ref cookie) = cookie {
        context.insert("message", cookie.value());
    }

    Template::render("zippyPage", &context)

}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
    .mount("/", routes![submit, index, zippyPage]).attach(Template::fairing())
}

fn main() {
    
    rocket().launch();
}
