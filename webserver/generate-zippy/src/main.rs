extern crate raster;
extern crate hex;
extern crate rand;
use std::str;
use raster::Image;
use rand::Rng;
use raster::Color;
use raster::editor;

//maybe use random numbers to add to each item in the random_bytes array?
fn generate_zippy_eyes(random_bytes: &mut [u8]){ 
    //randomize bytes
    random_bytes[0] = (random_bytes[0]%10)+100;
    random_bytes[1] = (random_bytes[1]%3)+200;
    random_bytes[2] = (random_bytes[2]%4)+150;

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy.png").unwrap();
    image.set_pixel(4,4, Color::rgb(random_bytes[0], random_bytes[1], random_bytes[2])).unwrap();
    image.set_pixel(8,4, Color::rgb(random_bytes[0], random_bytes[1], random_bytes[2])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_ears(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[3] = (random_bytes[3]%2)+15;
    random_bytes[4] = (random_bytes[4]%15)+34;
    random_bytes[5] = (random_bytes[5]%4)+23;

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(3,1, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(4,2, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(8,2, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(9,1, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_nose(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[6] = (random_bytes[6]%2)+15;
    random_bytes[7] = (random_bytes[7]%15)+34;
    random_bytes[8] = (random_bytes[8]%4)+23;

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(5,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(6,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(7,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(6,7, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_chest(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[9] = (random_bytes[9]%3)+242;
    random_bytes[10] = (random_bytes[10]%3)+242;
    random_bytes[11] = (random_bytes[11]%3)+242;

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(5,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(6,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(7,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(6,10, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_belly(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[12] = (random_bytes[12]%5)+3;
    random_bytes[13] = (random_bytes[13]%14)+163;
    random_bytes[14] = (random_bytes[14]%3)+29;

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(4,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(5,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(7,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(8,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(4,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(5,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(6,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(7,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(8,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_tail(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[15] = (random_bytes[15]+2);
    random_bytes[16] = (random_bytes[16]+12);
    random_bytes[17] = (random_bytes[17]+2);

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(14,9, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(15,9, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(14,10, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(15,10, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(12,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(13,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(14,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(11,12, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(12,12, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn generate_zippy_body(random_bytes: &mut [u8]){
    //randomize bytes
    random_bytes[18] = (random_bytes[15]+3);
    random_bytes[19] = (random_bytes[19]+12);
    random_bytes[0] - (random_bytes[0]%6)+15; //use index 0 because be don't have enough elements (20 items)

    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    image.set_pixel(3,0, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,0, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();

    image.set_pixel(9,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,6, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,6, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();

    image.set_pixel(6,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
}

fn main(){

    let mut random_bytes = rand::thread_rng().gen::<[u8; 20]>();
    let mut image = raster::open("C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png").unwrap();
    raster::save(&image, "C:/Users/david/OneDrive - The University of Akron/crypto-zippy/webserver/generate-zippy/zippy2.png");
    generate_zippy_eyes(&mut random_bytes); //calling function! works!

    generate_zippy_ears(&mut random_bytes);

    generate_zippy_nose(&mut random_bytes);

    generate_zippy_belly(&mut random_bytes);

    generate_zippy_body(&mut random_bytes);

    generate_zippy_chest(&mut random_bytes);

    generate_zippy_tail(&mut random_bytes);
}