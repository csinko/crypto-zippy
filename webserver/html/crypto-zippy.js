function GenerateNewWalletID(student_id) {
  //Generate anew RSA Key and get the public key string
var newRSAKey = cryptico.generateRSAKey(student_id, 512);
var publicKeyString = cryptico.publicKeyString(newRSAKey);

//Encrypt the public key string and store it in a cookie
var encryptedPublicKey = cryptico.encrypt(publicKeyString, publicKeyString);
  Cookies.set('encrypted_wallet_id', encryptedPublicKey);
  Cookies.set('wallet_id', publicKeyString);

  wallet_id_bytes = getWalletIDBytes(newRSAKey);
  //Generate the Zippy1 and Zippy2 hashes
  zippy1_hash = sha1.array(wallet_id_bytes.slice(0, 32));
  zippy2_hash = sha1.array(wallet_id_bytes.slice(32, 64));
  console.log("zip1hash", zippy1_hash);
  console.log("zip2hash", zippy2_hash);
  
  console.log("Wallet ID bytes", wallet_id_bytes);

  //Make a generation block
  data_block = [];
  //1 Byte for the ID type of the data (ID = 0x01)
  data_block.push(1);
  //64 bytes for the wallet ID, in bytes
  data_block = data_block.concat(wallet_id_bytes);
  data_block = data_block.concat(zippy1_hash);
  data_block = data_block.concat(zippy2_hash);

  //Get the signature of the datablock
  var data_block_signature  = getSignedMsgArray(newRSAKey, data_block);
  console.log("Data Block Signature", data_block_signature);
  data_block = data_block.concat(data_block_signature);

  //Now that the data block for wallet generation is formed, send it to the server
  //and try to get it posted to the blockchain

  //Put the data block data into a uint8 byte array
  var uint8array = new Uint8Array(data_block);
  var xhr = new XMLHttpRequest;
  xhr.open("POST", "http://localhost:8000/generateWallet", false);
  xhr.setRequestHeader('Content-Type', 'text/plain');
  xhr.send(uint8array);
  window.location.href = "http://localhost:8000/wallets/" + newRSAKey.n.toString(16);

}



function hexStringToByteArray(hexString) {
  if(hexString.length % 2 > 0) hexString = "0" + hexString;
  var byteArray = [];
  for(var i = 0; i < hexString.length; i += 2) {
    byteArray.push(parseInt(hexString.slice(i, i+2), 16));
  }
  return byteArray;
}

function getSignedMsgArray(rsaKey, msg) {
var signed_msg = rsaKey.signStringWithSHA256(msg);
  var hexString = signed_msg.toString(16);
  return hexStringToByteArray(hexString);
}

function getWalletIDBytes(rsaKey) {
  var hexString = rsaKey.n.toString(16);
  return hexStringToByteArray(hexString);
}

