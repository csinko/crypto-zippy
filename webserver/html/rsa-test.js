var password = "4139218";
var bits = 512;

console.log("Password: ", password);
console.log("Bits:", bits);

var newRSAKey = cryptico.generateRSAKey(password, bits);
console.log("RSA Key: ", newRSAKey)

var publicKeyString = cryptico.publicKeyString(newRSAKey);

//publicKeyString = n
//e is  ALWAYS 3 with this library

console.log("Public Key String: ", publicKeyString);


console.log("n: ", getRSANArray(newRSAKey));

var publicKeyID = cryptico.publicKeyID(publicKeyString);

console.log("Public Key ID: ", publicKeyID);

var msg = [1, 2, 3, 4, 5];

var signed_msg = getSignedMsgArray(newRSAKey, msg);
console.log("Signed msg: ", signed_msg);

function hexStringToByteArray(hexString) {
  if(hexString.length % 2 > 0) hexString = "0" + hexString;
  var byteArray = [];
  for(var i = 0; i < hexString.length; i += 2) {
    byteArray.push(parseInt(hexString.slice(i, i+2), 16));
  }
  return byteArray;
}

function getSignedMsgArray(rsaKey, msg) {
var signed_msg = rsaKey.signStringWithSHA256(msg);
  var hexString = signed_msg.toString(16);
  return hexStringToByteArray(hexString);
}

function getRSANArray(rsaKey) {
  var hexString = rsaKey.n.toString(16);
  return hexStringToByteArray(hexString);
}

