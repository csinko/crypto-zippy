#![feature(proc_macro_hygiene, decl_macro)]

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[link(name = "cryptozippy")]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;

#[macro_use] extern crate lazy_static;
extern crate crypto;

use rocket::data::Data;
//////////////////////////////////////////

extern crate raster;
extern crate hex;
extern crate rand;
use std::str;
use raster::Image;
use rand::Rng;
use raster::Color;
use raster::editor;
use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
use std::collections::HashMap;
use self::crypto::digest::Digest;
use self::crypto::sha1::Sha1;

//maybe use random numbers to add to each item in the random_bytes array?
fn generate_zippy_eyes(random_bytes: &mut [u8], filename: &str){ 
    //randomize bytes
    random_bytes[0] = (random_bytes[0]%10)+100;
    random_bytes[1] = (random_bytes[1]%3)+200;
    random_bytes[2] = (random_bytes[2]%4)+150;

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(4,4, Color::rgb(random_bytes[0], random_bytes[1], random_bytes[2])).unwrap();
    image.set_pixel(8,4, Color::rgb(random_bytes[0], random_bytes[1], random_bytes[2])).unwrap();
    raster::save(&image, filename);
}

fn generate_zippy_ears(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[3] = (random_bytes[3]%2)+15;
    random_bytes[4] = (random_bytes[4]%15)+34;
    random_bytes[5] = (random_bytes[5]%4)+23;

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(3,1, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(4,2, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(8,2, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    image.set_pixel(9,1, Color::rgb(random_bytes[3], random_bytes[4], random_bytes[5])).unwrap();
    raster::save(&image, filename);
}

fn generate_zippy_nose(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[6] = (random_bytes[6]%2)+15;
    random_bytes[7] = (random_bytes[7]%15)+34;
    random_bytes[8] = (random_bytes[8]%4)+23;

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(5,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(6,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(7,6, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    image.set_pixel(6,7, Color::rgb(random_bytes[6], random_bytes[7], random_bytes[8])).unwrap();
    raster::save(&image, filename);
}

fn generate_zippy_chest(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[9] = (random_bytes[9]%3)+242;
    random_bytes[10] = (random_bytes[10]%3)+242;
    random_bytes[11] = (random_bytes[11]%3)+242;

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(5,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(6,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(7,9, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    image.set_pixel(6,10, Color::rgb(random_bytes[9], random_bytes[10], random_bytes[11])).unwrap();
    raster::save(&image, filename); 
}

fn generate_zippy_belly(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[12] = (random_bytes[12]%5)+3;
    random_bytes[13] = (random_bytes[13]%14)+163;
    random_bytes[14] = (random_bytes[14]%3)+29;

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(4,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(5,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(7,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(8,11, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(4,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(5,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(6,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(7,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    image.set_pixel(8,12, Color::rgb(random_bytes[12], random_bytes[13], random_bytes[14])).unwrap();
    raster::save(&image, filename); 
}

fn generate_zippy_tail(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[15] = (random_bytes[15]+2);
    random_bytes[16] = (random_bytes[16]+12);
    random_bytes[17] = (random_bytes[17]+2);

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(14,9, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(15,9, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(14,10, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(15,10, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(12,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(13,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(14,11, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(11,12, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    image.set_pixel(12,12, Color::rgb(random_bytes[15], random_bytes[16], random_bytes[17])).unwrap();
    raster::save(&image, filename); 
}

fn generate_zippy_body(random_bytes: &mut [u8], filename: &str){
    //randomize bytes
    random_bytes[18] = (random_bytes[15]+3);
    random_bytes[19] = (random_bytes[19]+12);
    random_bytes[0] - (random_bytes[0]%6)+15; //use index 0 because be don't have enough elements (20 items)

    let mut image = raster::open(filename).unwrap();
    image.set_pixel(3,0, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,0, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,1, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,2, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,3, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,4, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();

    image.set_pixel(9,5, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,6, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,6, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,7, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,8, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,9, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,10, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();

    image.set_pixel(6,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,11, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,12, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(5,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(6,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(7,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,13, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(4,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(8,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,14, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(1,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(2,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(3,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(9,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(10,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    image.set_pixel(11,15, Color::rgb(random_bytes[18], random_bytes[19], random_bytes[0])).unwrap();
    raster::save(&image, filename);
}

//////////////////////////////////////////

struct GenBlockData {
    block_type: u8,
    wallet_id: [u8; 64],
    zippy1_id: [u8; 20],
    zippy2_id: [u8; 20],
    signature: [u8; 64],
}

struct Block {
    block_number: u64,
    data: GenBlockData,
    prev_hash: [u8; 20],
    hash: [u8; 20],

}



fn verify_message_signature(block: &GenBlockData) -> bool {
    let block_type : &[u8] = &[block.block_type];
    let msg_slice = [block_type, &block.wallet_id[..], &block.zippy1_id[..], &block.zippy2_id[..]].concat();
    let mut msg_array: [u8; 105] = [0; 105];
    msg_array.copy_from_slice(&msg_slice);

    let msg_array_data = msg_array.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let msg_go_slice = GoSlice {
        data: msg_array_data,
        len: 105,
        cap: 105,
    };
    ::std::mem::forget(msg_array);

    let mut sig_array = block.signature;
    let sig_array_data = sig_array.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let sig_go_slice = GoSlice {
        data: sig_array_data,
        len: 64,
        cap: 64,
    };
    ::std::mem::forget(sig_array);

    let mut n_array = block.wallet_id;
    let n_array_data = n_array.as_mut_ptr() as *mut ::std::os::raw::c_void;

    let n_go_slice = GoSlice {
        data: n_array_data,
        len: 64,
        cap: 64,
    };

    ::std::mem::forget(n_array);

    let e : ::std::os::raw::c_int = 3;

    let mut result : bool = false;

    unsafe {
        let res = VerifyMessage(msg_go_slice, sig_go_slice, n_go_slice, e);
        println!("Verified?: {}", res);
        if res == 1 {
        result = true;
        }
    }
   result
}

impl GenBlockData {
    pub fn new(data: &[u8]) -> GenBlockData {
    let mut new_block = GenBlockData {
        block_type: data[0],
        wallet_id: [0; 64],
        zippy1_id: [0; 20],
        zippy2_id: [0; 20],
        signature: [0; 64],
    };
        //new_block.block_type = data[0];
        new_block.wallet_id.copy_from_slice(&data[1..65]);
        new_block.zippy1_id.copy_from_slice(&data[65..85]);
        new_block.zippy2_id.copy_from_slice(&data[85..105]);
        new_block.signature.copy_from_slice(&data[105..]);
        new_block
    }
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

fn generate_zippy_image(zippy_hash :&mut[u8; 20]) {
    //Convert to a slice
    let mut zippy_hash_slice = &mut zippy_hash[..];
    //Make a file for the zippy
    let hex_string = hex::encode(zippy_hash_slice.to_vec());
    let mut main_image = raster::open("zippy.png").unwrap();
    let mut filename = "zippys/".to_string();
    filename.push_str(&hex_string);
    filename.push_str(&".png".to_string());
    raster::save(&main_image, &filename);
    generate_zippy_eyes(&mut zippy_hash_slice, &filename);
    generate_zippy_ears(&mut zippy_hash_slice, &filename);
    generate_zippy_nose(&mut zippy_hash_slice, &filename);
    generate_zippy_belly(&mut zippy_hash_slice, &filename);
    generate_zippy_body(&mut zippy_hash_slice, &filename);
    generate_zippy_chest(&mut zippy_hash_slice, &filename);
    generate_zippy_tail(&mut zippy_hash_slice, &filename);
}

#[post("/generateWallet", format = "plain", data = "<data>")]
fn generate_wallet(data: Data) -> String {
    let block : GenBlockData;
    if data.peek_complete() {
        let data_chunk = data.peek();
        block = GenBlockData::new(data_chunk);
        println!("Zippy 1 hash{:?}", block.zippy1_id);
        println!("Zippy 2 hash{:?}", block.zippy2_id);
        //Check that the wallet ID does not exist
        //
        //Check that the hashes are correct (1st half and 2nd half of wallet ID)

        //Ensure the signature is valid
        if !verify_message_signature(&block) {
            println!("Invalid Signature");
        }
    //generate the images
    let mut zippy1_id_clone = block.zippy1_id;
    let mut zippy2_id_clone =  block.zippy2_id;
    generate_zippy_image(&mut zippy1_id_clone);
    generate_zippy_image(&mut zippy2_id_clone);
    } else {
        block = GenBlockData{
            block_type: 0,
            wallet_id: [0; 64],
            zippy1_id: [0; 20],
            zippy2_id: [0; 20],
            signature: [0; 64],
        }
    }

    "OK".to_string()

}

#[get("/wallets/<wallet>")]
fn get_wallet(wallet: String) -> Template {
    let decoded_wallet = hex::decode(wallet.clone()).expect("Decoding Failed"); 
    println!("Decoded wallet: {:?}", decoded_wallet);
    //Get the hashes of the zippies
    let zippy1 = &decoded_wallet[0..32];
    let zippy2 = &decoded_wallet[32..64];
    let mut hasher = Sha1::new();
    hasher.input(zippy1);
    let zippy1_hash = hasher.result_str();
    hasher.reset();
    hasher.input(zippy2);
    let zippy2_hash = hasher.result_str();


    let mut context = HashMap::new();
    context.insert("zippy1", zippy1_hash);
    context.insert("zippy2", zippy2_hash);
    context.insert("wal_id", wallet);
    Template::render("wallet", &context)
}

fn main() {
    rocket::ignite().mount("/", routes![index, generate_wallet, get_wallet]).mount("/static", StaticFiles::from("zippys")).attach(Template::fairing()).launch();

}

