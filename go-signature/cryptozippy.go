package main

import "C"

import (
	"crypto/rsa"
	"math/big"
	"crypto/sha256"
	"crypto"
	"crypto/rand"
	"fmt"
	"os"
)

func main() {

	pivKey, err := rsa.GenerateKey(rand.Reader, 512)

	if err != nil {
		fmt.Println(err.Error)
		os.Exit(1)
	}

	//64 bits
	msg := []byte("AAAAAAAA")
	fmt.Printf("%d\n", msg)
	msgHash := sha256.Sum256(msg)
	signature, err := rsa.SignPKCS1v15(rand.Reader, pivKey, crypto.SHA256, msgHash[:])
	if err != nil {
		fmt.Println(err.Error)
		os.Exit(1)
	}
	fmt.Println("Signature")
	fmt.Println(signature)
	fmt.Println(len(signature))

	fmt.Println("Public Key N Bytes")
	fmt.Printf("%x\n", pivKey.PublicKey.N)
	fmt.Println(len(pivKey.PublicKey.N.Bytes()))

	fmt.Println("E")
	fmt.Println(pivKey.PublicKey.E)

	res := VerifyMessage(msg, signature, pivKey.PublicKey.N.Bytes(), C.int(pivKey.PublicKey.E))
	if res != true {
		fmt.Println("REEEEEEEE")
	} else {
		fmt.Println("All is good in the world")
	}
}



//export VerifyMessage
func VerifyMessage(msg []byte, signature []byte, n []byte, e C.int) bool {

	fmt.Println("msg")
	fmt.Println(msg)
	fmt.Println(len(msg))

	fmt.Println("Signature")
	fmt.Println(signature)
	fmt.Println(len(signature))

	fmt.Println("n")
	fmt.Println(n)
	fmt.Println(len(n))

	fmt.Println("e")
	fmt.Println(e)


	bigInt := new(big.Int)
	bigInt.SetBytes(n)

	msgHash := sha256.Sum256(msg)

	pubKey := &rsa.PublicKey {
		N: bigInt,
		E: int(e),
	}

	err :=  rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, msgHash[:], signature)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	return true

}
